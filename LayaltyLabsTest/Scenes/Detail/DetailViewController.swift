//
//  DetailViewController.swift
//  LayaltyLabsTest
//
//  Created by Евгений Котовщиков on 25/09/2019.
//  Copyright (c) 2019 Евгений Котовщиков. All rights reserved.
//

import UIKit

final class DetailViewController: UIViewController {
  weak var coordinator: FlowCoordinator?
  
  @IBOutlet weak var tableView: UITableView!
  
  // MARK: - Private Properties
  let contact: ContactModel
  
  // MARK: - Object Lifecycle
  init(contact: ContactModel) {
    self.contact = contact
    super.init(nibName: "DetailViewController", bundle: nil)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  // MARK: - View lifecycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    configureTableView()
  }
  
  // MARK: - Private Methods
  private func configureTableView() {
    tableView.dataSource = self
    tableView.register(UINib(nibName: "ContactsCell", bundle: nil), forCellReuseIdentifier: "ContactsCell")
  }
}

// MARK: - Display Logic

extension DetailViewController: UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 5
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let cell = tableView.dequeueReusableCell(withIdentifier: "ContactsCell", for: indexPath) as? ContactsCell else { return UITableViewCell() }
    
    switch indexPath.row {
    case 0:
      cell.nameLabel.text = contact.name
      cell.emailLabel.text = "UserName"
    case 1:
      cell.nameLabel.text = contact.phone
      cell.emailLabel.text = "Phone"
    case 2:
      if let adress = contact.address,
        let zipcode = adress.zipcode,
        let city = adress.city,
        let street = adress.street,
        let suite = adress.suite {
        cell.nameLabel.text = "\(zipcode) \(city) \(street) \(suite)"
      }
      cell.emailLabel.text = "Adress"
    case 3:
      cell.nameLabel.text = contact.website
      cell.emailLabel.text = "Website"
    case 4:
      cell.nameLabel.text = contact.company?.name
      cell.emailLabel.text = "Company"
    default:
      break
    }
    return cell
  }
}
