//
//  DetailBuilder.swift
//  LayaltyLabsTest
//
//  Created by Евгений Котовщиков on 25/09/2019.
//  Copyright (c) 2019 Евгений Котовщиков. All rights reserved.
//

import UIKit

final class DetailBuilder {
  
  // MARK: - Private Properties
  var contact: ContactModel?
  
  // MARK: - Public Methods
  
  func set(contact: ContactModel) -> DetailBuilder {
    self.contact = contact
    return self
  }
  
  func build() -> UIViewController {
    
    guard let contact = self.contact else {
      fatalError("Didn't set contact :(")
    }
    
    let controller = DetailViewController(contact: contact)
    
    return controller
  }
}
