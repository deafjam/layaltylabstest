//
//  ContactsDataFlow.swift
//  LayaltyLabsTest
//
//  Created by Евгений Котовщиков on 25/09/2019.
//  Copyright (c) 2019 Евгений Котовщиков. All rights reserved.
//

import UIKit
import Alamofire

enum Contacts {
  // MARK: - Use cases
  enum Sort {
    struct Request {
      var ascending: Bool
    }
  }
  
  enum Promise {
    struct Request {
    }
    
    struct Response {
      var result: [ContactModel]
    }
    
    struct ViewModel {
      var state: ViewControllerState
    }
  }
  
  enum ViewControllerState {
    case initial
    case result([ContactModel])
    case emptyResult
    case failure(error: ContactsError)
  }
  
  enum ContactsError: Error {
    case someError(message: String)
  }
}
