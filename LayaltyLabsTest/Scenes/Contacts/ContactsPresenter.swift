//
//  ContactsPresenter.swift
//  LayaltyLabsTest
//
//  Created by Евгений Котовщиков on 25/09/2019.
//  Copyright (c) 2019 Евгений Котовщиков. All rights reserved.
//

import UIKit

protocol ContactsPresentationLogic {
  func presentContacts(response: Contacts.Promise.Response)
}

final class ContactsPresenter: ContactsPresentationLogic {
  weak var viewController: ContactsDisplayLogic?
  
  // MARK: - Present contacts
  
  func presentContacts(response: Contacts.Promise.Response) {
    let viewModel = Contacts.Promise.ViewModel(state: .result(response.result))
    viewController?.displayContacts(viewModel: viewModel)
  }
}
