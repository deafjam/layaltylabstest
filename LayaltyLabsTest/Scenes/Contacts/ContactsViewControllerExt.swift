//
//  ContactsViewControllerExt.swift
//  LayaltyLabsTest
//
//  Created by Евгений Котовщиков on 27/09/2019.
//  Copyright © 2019 Евгений Котовщиков. All rights reserved.
//

import UIKit

extension ContactsViewController {
  
  func sortView() {
    let firstButton = UIAlertAction(title: ButtonChecked.ascending.rawValue, style: .default) { [unowned self] _ in
      self.buttonChecked = .ascending
    }
    firstButton.setValue(UIColor.black, forKey: "titleTextColor")
    
    let secondButton = UIAlertAction(title: ButtonChecked.descending.rawValue, style: .default) { [unowned self] _ in
      self.buttonChecked = .descending
    }
    secondButton.setValue(UIColor.black, forKey: "titleTextColor")
    
    let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
    
    switch buttonChecked {
    case .ascending:
      firstButton.setValue(true, forKey: "checked")
    case .descending:
      secondButton.setValue(true, forKey: "checked")
    }
    
    let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
    
    alert.addAction(firstButton)
    alert.addAction(secondButton)
    alert.addAction(cancel)
    self.present(alert, animated: true, completion: nil)
  }
}
