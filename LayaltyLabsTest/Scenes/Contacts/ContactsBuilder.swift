//
//  ContactsBuilder.swift
//  LayaltyLabsTest
//
//  Created by Евгений Котовщиков on 25/09/2019.
//  Copyright (c) 2019 Евгений Котовщиков. All rights reserved.
//

import UIKit

final class ContactsBuilder {
  
  // MARK: - Private Properties
  private var initialState: Contacts.ViewControllerState?
  
  // MARK: - Public Methods
  func set(initialState: Contacts.ViewControllerState) -> ContactsBuilder {
    self.initialState = initialState
    return self
  }
  
  func build() -> UIViewController {
    let presenter = ContactsPresenter()
    let interactor = ContactsInteractor(presenter: presenter)
    let controller = ContactsViewController(interactor: interactor)
    
    presenter.viewController = controller
    return controller
  }
}
