//
//  ContactsViewController.swift
//  LayaltyLabsTest
//
//  Created by Евгений Котовщиков on 25/09/2019.
//  Copyright (c) 2019 Евгений Котовщиков. All rights reserved.
//

import UIKit

protocol ContactsDisplayLogic: class {
  func displayContacts(viewModel: Contacts.Promise.ViewModel)
}

final class ContactsViewController: UIViewController {
  weak var coordinator: FlowCoordinator?
  
  enum ButtonChecked: String {
    case ascending = "Sort by A-Z"
    case descending = "Sort by Z-A"
  }
  
  var ascending: Bool = true
  
  var buttonChecked: ButtonChecked = .ascending {
    didSet {
      switch buttonChecked {
      case .ascending:
        ascending = true
      case .descending:
        ascending = false
      }
      let request = Contacts.Sort.Request.init(ascending: ascending)
      interactor.sort(request: request)
    }
  }
  
  @IBOutlet weak var tableView: UITableView!
  
  // MARK: - Private Properties
  private let interactor: ContactsBusinessLogic
  private var state: Contacts.ViewControllerState
  private var contacts: [ContactModel] = []
  
  // MARK: - Object Lifecycle
  init(interactor: ContactsBusinessLogic, initialState: Contacts.ViewControllerState = .initial) {
    self.interactor = interactor
    self.state = initialState
    super.init(nibName: "ContactsViewController", bundle: nil)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  // MARK: - View lifecycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    configureNavigationBar()
    configureTableView()
    fetchData()
  }
  
  private func configureNavigationBar() {
    self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "sort"), style: .done, target: self, action: #selector(didInfoTapped))
  }
  
  private func configureTableView() {
    tableView.dataSource = self
    tableView.delegate = self
    tableView.register(UINib(nibName: "ContactsCell", bundle: nil), forCellReuseIdentifier: "ContactsCell")
  }
  
  // MARK: - Private Methods
  private func fetchData() {
    let request = Contacts.Promise.Request()
    interactor.fetch(request: request)
  }
  
  @objc func didInfoTapped() {
    sortView()
  }
}

// MARK: - Display Logic
extension ContactsViewController: ContactsDisplayLogic {
  func displayContacts(viewModel: Contacts.Promise.ViewModel) {
    display(newState: viewModel.state)
  }
  
  func display(newState: Contacts.ViewControllerState) {
    state = newState
    switch state {
    case .initial:
      print("initial state")
      
    case let .failure(error):
      print("error \(error)")
      
    case let .result(items):
      self.contacts = items
      DispatchQueue.main.async {
        self.tableView.reloadData()
      }
      
    case .emptyResult:
      print("empty result")
      
    }
  }
}

extension ContactsViewController: UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    contacts.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let cell = tableView.dequeueReusableCell(withIdentifier: "ContactsCell", for: indexPath) as? ContactsCell else { return UITableViewCell() }
    cell.configureCell(model: contacts[indexPath.row])
    
    return cell
  }
}

extension ContactsViewController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    coordinator?.goToDetails(contact: contacts[indexPath.row])
  }
}
