//
//  ContactsInteractor.swift
//  LayaltyLabsTest
//
//  Created by Евгений Котовщиков on 25/09/2019.
//  Copyright (c) 2019 Евгений Котовщиков. All rights reserved.
//

import UIKit
import PromiseKit
import RealmSwift

protocol ContactsBusinessLogic {
  func sort(request: Contacts.Sort.Request)
  func fetch(request: Contacts.Promise.Request)
}

final class ContactsInteractor: ContactsBusinessLogic {
  
  // MARK: - Private Properties
  private let presenter: ContactsPresentationLogic
  
  // MARK: - Object Lifecycle
  init(presenter: ContactsPresentationLogic) {
    self.presenter = presenter
  }
  
  // MARK: - Public methods
  
  func fetch(request: Contacts.Promise.Request) {
    
    firstly {
      DBClient.getAllData(as: ContactModel.self)
      
    }.then { [weak self] result -> Promise<[ContactModel]> in
      let response = Contacts.Promise.Response.init(result: Array(result))
      self?.presenter.presentContacts(response: response)
    
      return APIClient.fetchContactsPromise()
      
    }.then { [weak self] result -> Promise<[ContactModel]> in
      let response = Contacts.Promise.Response.init(result: Array(result))
      self?.presenter.presentContacts(response: response)
      
      return DBClient.storeAllData(objects: result)
      
    }.catch { (error) in
      debugPrint(error.localizedDescription)
    }
  }
  
  func sort(request: Contacts.Sort.Request) {
    firstly {
      DBClient.getSortedData(as: ContactModel.self, ascending: request.ascending)
      
    } .done { [weak self] result in
      let response = Contacts.Promise.Response.init(result: Array(result))
      self?.presenter.presentContacts(response: response)
      
    }.catch { (error) in
      debugPrint(error.localizedDescription)
    }
  }
}
