//
//  ContactsCell.swift
//  LayaltyLabsTest
//
//  Created by Евгений Котовщиков on 26/09/2019.
//  Copyright © 2019 Евгений Котовщиков. All rights reserved.
//

import UIKit

class ContactsCell: UITableViewCell {
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var emailLabel: UILabel!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
  }
  
  public func configureCell(model: ContactModel) {
    if let name = model.name {
      nameLabel.text = name
    }
    
    if let email = model.email {
      emailLabel.text = email
    }
  }
}
