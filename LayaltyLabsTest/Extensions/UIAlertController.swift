//
//  UIAlertController.swift
//  LayaltyLabsTest
//
//  Created by Евгений Котовщиков on 25/09/2019.
//  Copyright © 2019 Евгений Котовщиков. All rights reserved.
//

import UIKit

extension UIAlertController {
  static func actionSheetWithItems<A : Equatable>(items : [(title : String, value : A)], currentSelection : A? = nil, action : @escaping (A) -> Void) -> UIAlertController {
    let controller = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
    for (var title, value) in items {
      if let selection = currentSelection, value == selection {
        // Note that checkmark and space have a neutral text flow direction so this is correct for RTL
        title = title + "✔︎ "
      }
      controller.addAction(
        UIAlertAction(title: title, style: .default) {_ in
          action(value)
        }
      )
    }
    return controller
  }
}
