//
//  ContactModel.swift
//  LayaltyLabsTest
//
//  Created by Евгений Котовщиков on 25/09/2019.
//  Copyright © 2019 Евгений Котовщиков. All rights reserved.
//

import Foundation
import RealmSwift

class ContactModel: Object, Codable {
  @objc dynamic var id: Int = 0
  @objc dynamic var name: String?
  @objc dynamic var username: String?
  @objc dynamic var email: String?
  @objc dynamic var address: AddressModel?
  @objc dynamic var phone: String?
  @objc dynamic var website: String?
  @objc dynamic var company: CompanyModel?
  
  override class func primaryKey() -> String? {
      return "id"
  }
}
