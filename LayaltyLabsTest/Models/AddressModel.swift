//
//  AddressModel.swift
//  LayaltyLabsTest
//
//  Created by Евгений Котовщиков on 25/09/2019.
//  Copyright © 2019 Евгений Котовщиков. All rights reserved.
//

import Foundation
import RealmSwift

class AddressModel: Object, Codable {
  @objc dynamic var street: String?
  @objc dynamic var suite: String?
  @objc dynamic var city: String?
  @objc dynamic var zipcode: String?
  @objc dynamic var geo: GeoModel?
}
