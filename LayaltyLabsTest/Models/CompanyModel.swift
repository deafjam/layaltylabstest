//
//  CompanyModel.swift
//  LayaltyLabsTest
//
//  Created by Евгений Котовщиков on 25/09/2019.
//  Copyright © 2019 Евгений Котовщиков. All rights reserved.
//

import Foundation
import RealmSwift

class CompanyModel: Object, Codable {
  @objc dynamic var name: String?
  @objc dynamic var catchPhrase: String?
  @objc dynamic var bs: String?
  
  private enum CodingKeys: String, CodingKey {
    case name
    case catchPhrase
    case bs
  }
}
