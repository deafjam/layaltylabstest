//
//  GeoModel.swift
//  LayaltyLabsTest
//
//  Created by Евгений Котовщиков on 25/09/2019.
//  Copyright © 2019 Евгений Котовщиков. All rights reserved.
//

import Foundation
import RealmSwift

class GeoModel: Object, Codable {
  @objc dynamic var lat: String?
  @objc dynamic var lng: String?
  
  private enum CodingKeys: String, CodingKey {
    case lat
    case lng
  }
}
