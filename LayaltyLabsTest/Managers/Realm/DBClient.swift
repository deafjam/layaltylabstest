//
//  DBClient.swift
//  LayaltyLabsTest
//
//  Created by Евгений Котовщиков on 26/09/2019.
//  Copyright © 2019 Евгений Котовщиков. All rights reserved.
//

import RealmSwift
import PromiseKit

class DBClient {
  
  fileprivate init() { }
  
  static var realm: Realm {
    let config = Realm.Configuration(deleteRealmIfMigrationNeeded: true)
    do {
      return try Realm(configuration: config)
    } catch {
      fatalError("realm not reachable")
    }
  }
  
  static func getAllData<T: Object>(as type: T.Type) -> Promise<Results<T>> {
    
    return Promise { seal in
      seal.fulfill(realm.objects(T.self))
    }
  }
  
  static func storeAllData<T: Object>(objects: [T]) -> Promise<[T]> {
    return Promise { seal in
      do {
        try realm.write {
          realm.add(objects, update: .modified)
          seal.fulfill(objects)
          debugPrint("object is stored")
        }
      } catch let error {
        debugPrint("error: \(error)")
        seal.reject(error)
      }
    }
  }
  
  static func getSortedData<T: Object>(as type: T.Type,
                         ascending: Bool) -> Promise<Results<T>> {
    
    return Promise { seal in
      seal.fulfill(realm.objects(T.self).sorted(byKeyPath: "name", ascending: ascending))
    }
  }
  
  static func deleteAllDatabase()  {
    do {
      try realm.write {
        realm.deleteAll()
      }
    } catch let error {
      debugPrint("error: \(error)")
    }
  }
  
  static func deleteData<T: Object>(object: T) {
    do {
      try realm.write {
        realm.delete(object)
      }
    } catch let error {
      debugPrint("error: \(error)")
    }
  }
}
