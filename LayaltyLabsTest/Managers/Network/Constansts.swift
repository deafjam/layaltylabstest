//
//  Constansts.swift
//  LayaltyLabsTest
//
//  Created by Евгений Котовщиков on 25/09/2019.
//  Copyright © 2019 Евгений Котовщиков. All rights reserved.
//

import Foundation

import Alamofire

enum HTTPHeaderField: String {
//  case authentication = "Authorization"
  case contentType = "Content-Type"
  case acceptType = "Accept"
//  case acceptEncoding = "Accept-Encoding"
}

enum ContentType: String {
  case json = "application/json"
}
