//
//  APIRouter.swift
//  LayaltyLabsTest
//
//  Created by Евгений Котовщиков on 25/09/2019.
//  Copyright © 2019 Евгений Котовщиков. All rights reserved.
//

import Alamofire

enum APIRouter: URLRequestConvertible {
  
  case getContacts
  
  // MARK: - HTTPMethod
  private var method: HTTPMethod {
    switch self {
    case .getContacts:
      return .get
    }
  }
  
  private var path: String {
    switch self {
      
    case .getContacts:
      return "/users"
    }
  }
  
  // MARK: - Parameters
  var parameters: Parameters? {
    switch self {
    case .getContacts:
      return nil
    }
  }
  
  // MARK: - URLRequestConvertible
  func asURLRequest() throws -> URLRequest {
    let url = URLBuilder().set(path: path).buildURL()!
    
    var urlRequest = URLRequest(url: url)
    urlRequest.timeoutInterval = 10
    
    // HTTP Method
    urlRequest.httpMethod = method.rawValue
    
    // Common Headers
    urlRequest.setValue(ContentType.json.rawValue, forHTTPHeaderField: HTTPHeaderField.acceptType.rawValue)
    urlRequest.setValue(ContentType.json.rawValue, forHTTPHeaderField: HTTPHeaderField.contentType.rawValue)
    
    // Parameters
    if let parameters = parameters, method != .get {
      do {
        urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
      } catch {
        throw AFError.parameterEncodingFailed(reason: .jsonEncodingFailed(error: error))
      }
    } else {
      urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
    }
    
    return urlRequest
  }
}

