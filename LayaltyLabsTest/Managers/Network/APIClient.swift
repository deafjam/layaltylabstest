//
//  APIClient.swift
//  LayaltyLabsTest
//
//  Created by Евгений Котовщиков on 25/09/2019.
//  Copyright © 2019 Евгений Котовщиков. All rights reserved.
//

import Alamofire
import PromiseKit

class APIClient {
  
  static func fetchContactsPromise() -> Promise<[ContactModel]> {
    return Promise { seal in
      AF.request(APIRouter.getContacts)
        .validate()
        .responseData { (response) in
          switch response.result {
          case .success(let data):
            let decoder = JSONDecoder()
            do {
              let result = try decoder.decode([ContactModel].self, from: data)
              seal.fulfill(result)
            } catch(let error) {
              seal.reject(error)
            }
            
          case .failure(let error):
            seal.reject(error)
          }
      }
    }
  }
}
