//
//  FlowCoordinator.swift
//  LayaltyLabsTest
//
//  Created by Евгений Котовщиков on 25/09/2019.
//  Copyright © 2019 Евгений Котовщиков. All rights reserved.
//

import UIKit

class FlowCoordinator: Coordinator {
  
  unowned let navigationController: UINavigationController
  
  required init(navigationController: UINavigationController) {
    self.navigationController = navigationController
  }
  
  func setupNavController() {
    navigationController.navigationBar.isHidden = false
  }
  
  func start() {
    guard let controller = ContactsBuilder().build() as? ContactsViewController else {
      fatalError("Couldn't cast view controller to ContactsController :(")
    }
    
    controller.coordinator = self
    navigationController.show(controller, sender: nil)
    setupNavController()
  }
  
  func goToDetails(contact: ContactModel) {
    guard let controller = DetailBuilder()
      .set(contact: contact)
      .build() as? DetailViewController else {
        fatalError("Couldn't cast view controller to DetailViewController :(")
    }
    controller.coordinator = self
    navigationController.show(controller, sender: nil)
  }
  
  func goToRoot() {
    navigationController.popToRootViewController(animated: true)
  }
}
